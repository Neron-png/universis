import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {AngularDataContext} from '@themost/angular';
import {Subscription} from 'rxjs';


@Component({
  selector: 'app-students-scholarships',
  templateUrl: './students-scholarships.component.html',
  styleUrls: ['./students-scholarships.component.scss']
})
export class StudentsScholarshipsComponent implements OnInit, OnDestroy  {
  public model: any;
  private subscription: Subscription;

  constructor(private _element: ElementRef,
              private _activatedRoute: ActivatedRoute,
              private _translate: TranslateService,
              private _context: AngularDataContext) {  }


  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
    this.model = await this._context.model('StudentScholarships')
      .where('student').equal(params.id)
      .expand('scholarship($expand=scholarshipType,status,department,year)')
      .getItems();
     });
    }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
