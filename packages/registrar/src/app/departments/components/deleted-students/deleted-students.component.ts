import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable, Subscription} from 'rxjs';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '@universis/ngx-tables';
import {AdvancedSearchFormComponent} from '@universis/ngx-tables';
import {ActivatedUser, AuthGuard, ErrorService, LoadingService, ModalService} from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import {AngularDataContext} from '@themost/angular';
import {AdvancedTableSearchComponent} from '@universis/ngx-tables';
import { ActivatedTableService } from '@universis/ngx-tables';
import {AdvancedRowActionComponent} from '@universis/ngx-tables';
import {ClientDataQueryable} from '@themost/client';
import {ActiveDepartmentService} from '../../../registrar-shared/services/activeDepartmentService.service';
import { DiagnosticsService } from '@universis/common';
import * as DELETED_STUDENTS_CONFIG from './deleted-students.config.list.json';
import * as SEARCH_CONFIG from './deleted-students-table.search.list.json';


@Component({
  selector: 'app-deleted-students',
  templateUrl: './deleted-students.component.html',
  styles: []
})
export class DeletedStudentsComponent implements OnInit, OnDestroy {
  private selectedItems = [];
  private dataSubscription: Subscription;
  private paramSubscription: Subscription;
  private reloadSubscription: Subscription;
  public isLoading = true;

  @Input() department: any;
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();
  public recordsTotal: any;


  constructor(
    private _activatedRoute: ActivatedRoute,
    private _translateService: TranslateService,
    private _context: AngularDataContext,
    private _activatedTable: ActivatedTableService,
    private _modalService: ModalService,
    private _loadingService: LoadingService,
    private _errorService: ErrorService,
    private _activeDepartmentService: ActiveDepartmentService,
    private _diagnosticsService: DiagnosticsService,
    private _authGuard: AuthGuard,
    private _activatedUser: ActivatedUser,
    private _router: Router
  ) {
  }


  ngOnInit() {
    this.isLoading = true;
    this._activatedTable.activeTable = this.table;
    this.table.config = AdvancedTableConfiguration.cast(DELETED_STUDENTS_CONFIG);
    this.search.form = SEARCH_CONFIG;
    Object.assign(this.search.form, {department: this._activatedRoute.snapshot.data.department.id});
    this.search.ngOnInit();
    this.advancedSearch.text = null;

    // reset table
    this.table.reset(false);
    // do reload by using hidden fragment e.g. /classes#reload
    this.reloadSubscription = this._activatedRoute.fragment.subscribe(fragment => {
      if (fragment && fragment === 'reload') {
        this.table.fetch(true);
      }
    });
  }

  /**
   * Restore student from selected items
   */
  async restoreStudent() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      this.selectedItems = items;
      // get only not restored items
      this.selectedItems = items.filter(x => {
        return x.revision_tag !== 'restored';
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          formTemplate: this.selectedItems && this.selectedItems.length > 0 ? 'Students/restore' : null,
          modalTitle: 'RestoreStudents.Restore.Title',
          description: 'RestoreStudents.Restore.Description',
          errorMessage: 'RestoreStudents.Restore.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeRestoreStudentAction()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  /**
   * Executes createStudent action for candidates
   */
  executeRestoreStudentAction() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // get values from modal component
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;

      // get values from modal component
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            const student = {
              'revision_object': item.revision_object,
              'calculateIdentifiers': data.calculateIdentifiers
            };
            await this._context.model(`Students/restore`).save(student);
            result.success += 1;
            try {
              await this.table.fetchOne({
                id: item.id
              });
            } catch (err) {
              //
            }
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async getSelectedItems() {
    let items = [];
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          // get items
          const selectArguments = ['id', 'studentStatus/alternateName as status', 'semester',
            'revision_tag', 'revision_object'];
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter(item => {
              return this.table.unselected.findIndex((x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.table.selected.map((item) => {
            return {
              id: item.id,
              status: item.studentStatus,
              semester: item.semester,
              revision_tag: item.revision_tag,
              revision_object: item.revision_object
            };
          });
        }
      }
    }
    return items;
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
    this.isLoading = false;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.reloadSubscription) {
      this.reloadSubscription.unsubscribe();
    }
  }
}
