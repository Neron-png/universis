import { AfterViewInit, Component, OnDestroy, ViewChild, ViewEncapsulation } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { AngularDataContext } from "@themost/angular";
import { ErrorService, LoadingService, UserActivityService } from "@universis/common";
import { AdvancedFormComponent, EmptyValuePostProcessor } from '@universis/forms';
import { first } from "rxjs/operators";

@Component({
    selector: 'app-inspect-student-degree-component',
    templateUrl: './inspect-student-degree.component.html',
    encapsulation: ViewEncapsulation.None,
    styles: [
        `
        .formio-component-panel.border-0 .card {
            border: none !important;
        }
        .formio-component-textarea .card-body {
            box-shadow: none !important;
            border: 1px solid #E4E7EA;
        }
        .card-form .card {
            border: none !important;
            box-shadow: none !important;
        }
        `
    ]
})
export class InspectStudentDegreeComponent implements AfterViewInit, OnDestroy {

    public results: any[];

    public showSearch = true;

    @ViewChild('formComponent') formComponent: AdvancedFormComponent;

    constructor(private context: AngularDataContext,
        private loading: LoadingService,
        private errorService: ErrorService,
        private userActivityService: UserActivityService,
        private translateService: TranslateService,
        private activatedRoute: ActivatedRoute ) {
    }

    ngOnDestroy(): void {
        
    }
    ngAfterViewInit(): void {
        if (this.formComponent) {
            this.formComponent.form.submissionLoad.subscribe((event: any) => {
                this.setFocus('data[ssn]');
                this.activatedRoute.queryParams.pipe(first()).subscribe((queryParams) => {
                    if (queryParams.student) {
                        void this.context.model('Students').select(
                            'person/givenName as firstName',
                            'person/familyName as lastName',
                            'person/fatherName as fatherName',
                            'person/motherName as motherName',
                            'person/birthDate as birthDate',
                            'person/ssn as ssn',
                            'person/vatNumber as taxId'
                        ).where('id').equal(queryParams.student).getItem().then((data) => {
                            if (data) {
                                this.formComponent.refreshForm.emit({
                                    submission: {
                                        data
                                      }
                                });
                            }
                        });
                    }
                });
            });
        }
        this.userActivityService.setItem({
            category: this.translateService.instant('Students.StudentTitle'),
            description: this.translateService.instant('DataAvailabilityAttributes.Validation.Search'),
            url: window.location.hash.substring(1),
            dateCreated: new Date()
          });
    }

    private inspectDegree(data: any) {
        return this.context.getService().execute({
            method: 'POST',
            headers: {},
            url: 'Students/InspectDegree',
            data: data
        });
    }

    private setFocus(elementName: string) {
        try {
            const element = document.getElementsByName(elementName)[0];
                if (element) {
                    element.focus();
                }
        } catch {
            // do nothing
        }
    }

    onCustomEvent(event: any) {
        if (event.type === 'clear') {
            this.formComponent.refreshForm.emit({
                submission: {
                    data: {}
                  }
            });
            this.results = null;
            this.setFocus('data[ssn]');
        }
        if (event.type === 'search') {
            this.loading.showLoading();
            const data = event.data;
            // delete form properties
            delete data.submit;
            // remove empty values
            Object.keys(data).forEach((key:string) => {
                if (Object.prototype.hasOwnProperty.call(data, key)) {
                    if (typeof data[key] === 'string') {
                        if (data[key].length === 0) {
                            delete data[key];
                        }
                    }
                }
            });
            // get degrees
            this.inspectDegree(data).then((item) => {
                if (item && item.degrees) {
                    this.results = item.degrees;
                } else {
                    this.results = [];
                }
                this.loading.hideLoading();
            }).then(() => {
                setTimeout(() => {
                    this.showSearch = false;
                }, 250);
            }).catch((error) => {
                this.loading.hideLoading();
                this.errorService.showError(error, {
                    continueLink: '.'
                });
            });
        }
    }


}
